﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Mueve el objeto que tenga este script en formato sinoidal con una seed que hace que varie el movimiento
/// </summary>
public class PlatformMovement : MonoBehaviour{

	Vector2 startPos;
	float seed;

	void Start() {
		startPos = transform.position;
		seed = Random.Range(0, 1000);
	}

	void Update(){
		transform.position = startPos + new Vector2(0, Mathf.Sin(Time.time+seed)/4f);
	}
}
