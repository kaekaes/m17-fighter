﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour{

	public Player myPlayer;
	bool heGolpiao;

	public void Start() {
		heGolpiao = false;
		Destroy(this.gameObject, 4f);
		if (GetComponent<Rigidbody2D>().velocity.x < 0)
			GetComponent<SpriteRenderer>().flipX = true;
	}

	/// <summary>
	/// Si golpea a un enemigo hace lo mismo que OnAttack (la función de las animaciones)
	/// </summary>
	/// <param name="collision"></param>
	public void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.layer == LayerMask.NameToLayer("Players") && !collision.gameObject.Equals(myPlayer.gameObject) && !heGolpiao) {
			heGolpiao = true;
			myPlayer.HitEnemy(4, collision.gameObject);
			int extraHdc = Random.Range(1, 11);
			collision.gameObject.GetComponent<Entity>().AddHandicap(extraHdc);
			Destroy(this.gameObject, 0.02f);
		}
		if (collision.gameObject.CompareTag("Suelo")) {
			Destroy(this.gameObject);
		}
	}
}
